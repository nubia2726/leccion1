$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
      interval: 2000
    });
    $('#contacto').on('show.bs.modal', function (e){
        console.log('el modal comienza abrir');

        $('#contactobtn').removeClass ('btn-prymary');
        $('#contactobtn').addClass('btn-danger');
        $('#contactobtn').prop('disable',true)

    });
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('el modal termina de abrir');
    });
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('el modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('el modal se oculto'); 

        $('#contactobtn').removeClass ('btn-danger');
        $('#contactobtn').addClass('btn-primary');
        $('#contactobtn').prop('disable',false)
    })

});